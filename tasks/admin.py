from django.contrib import admin
from django import forms
from .models import Task
from django.contrib.admin.widgets import AdminDateWidget, AdminTimeWidget

class TaskForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        # only change attributes if an instance is passed            
        instance = kwargs.get('instance')
        if instance:
            self.base_fields['event_date'].initial = instance.initial_date.strftime("%Y-%m-%d")
            self.base_fields['initial_time'].initial = instance.initial_date.strftime("%H:%M:%S")
            self.base_fields['end_time'].initial = instance.end_date.strftime("%H:%M:%S")
        forms.ModelForm.__init__(self, *args, **kwargs)

    MY_CHOICES = (
        ('Principal', 'Principal'),
        ('Galileo', 'Galileo'),
        ('Miguel Angel', 'Miguel Angel'),
        ('Gutenberg', 'Gutenberg'),
        ('Pitagoras', 'Pitagoras'),
        ('Arquimedes', 'Arquimedes'),
        ('Hipatia', 'Hipatia'),
        ('Stadium', 'Stadium'),
    )

    venue = forms.ChoiceField(choices=MY_CHOICES)
    name = forms.CharField()
    exhibitor = forms.CharField()
    description = forms.CharField(widget=forms.Textarea)
    event_date = forms.DateField(widget=AdminDateWidget)
    initial_time = forms.TimeField(widget=AdminTimeWidget)
    end_time = forms.TimeField(widget=AdminTimeWidget)

class TaskAdmin(admin.ModelAdmin):

    search_fields = ['name', 'description', 'venue']
    list_display = (
        'name',
        'exhibitor',
        'description',
        'initial_date',
        'end_date',
        'venue',
        )
    list_filter = (
        'name',
        'exhibitor',
        'description',
        'initial_date',
        'end_date',
        'venue',
        )
    fields = (
        'name',
        'exhibitor',
        'description',
        'event_date',
        'initial_time',
        'end_time',
        'venue',
        )

    form = TaskForm

    def save_model(self, request, obj, form, change):
        if form.is_valid():
            from datetime import datetime
            obj.initial_date = datetime.combine(form.cleaned_data['event_date'], form.cleaned_data['initial_time'])
            obj.end_date = datetime.combine(form.cleaned_data['event_date'], form.cleaned_data['end_time'])
            obj.save()

admin.site.register(Task, TaskAdmin)