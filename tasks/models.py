from django.db import models

# Create your models here.
class Task(models.Model):

    name = models.CharField(max_length=200)
    description = models.TextField()
    exhibitor = models.CharField(max_length=200, blank=True)
    initial_date = models.DateTimeField()
    end_date = models.DateTimeField()
    venue = models.CharField(max_length=200)
